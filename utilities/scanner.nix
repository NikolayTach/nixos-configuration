{ lib }:

/* Helper to scan folders

   Import example:
   let
     inherit (import ./scanner.nix { inherit lib; }) importNixFolder;

     ...
   in
*/
rec {
  /* Return the list of files from the folder

     Example:
       scanFolder ../desktop
       => [ ".config.nix" "email.nix" ... ]
  */
  scanFolder = folder: lib.attrNames (builtins.readDir folder);

  /* Filter the strings that ends with "nix"

     Example:
       filterNixFiles [ "test.nix", "not_valid.test" ]
       => [ "test.nix" ]
  */
  filterNixFiles = (builtins.filter (fileName: lib.hasSuffix "nix" fileName));

  /* Return the list of Nix files from the folder

     Example:
       getNixFiles ../desktop
       => [ ".config.nix" "email.nix" ... ]
  */
  getNixFiles = path: filterNixFiles (scanFolder path);

  /* Return the list of Nix files from the folder, ready to be added to your imports

     Example:
       importNixFolder ../desktop
       => [ ../desktop/.config.nix ../desktop/email.nix ... ]
  */
  importNixFolder = folder:
    map (file: folder + "/${file}") (getNixFiles folder);
}
