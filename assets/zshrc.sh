# Replace context
prompt_context() {
	# Custom (Random emoji)
	emojis=("🔥" "💀" "👑" "😎" "🐸" "🐵" "🦄" "🌈" "🍻" "🚀" "💡" "🎉" "🔑" "🚦" "🌙")
	RAND_EMOJI_N=$(( $RANDOM % ${#emojis[@]} + 1))
	CONTEXT="${emojis[$RAND_EMOJI_N]}"

	if [ ! -z ${SSH_CLIENT+x} ]; then
		CONTEXT="$CONTEXT $HOST"
	fi

    prompt_segment black default "$CONTEXT"
}

# Replace directory
# Dir: current working directory
prompt_dir() {
	prompt_segment blue black '%c'
}

# Enable direnv
eval "$(direnv hook zsh)"
