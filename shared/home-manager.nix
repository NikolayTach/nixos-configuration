{ config, pkgs, ... }:

let
  home-manager = builtins.fetchGit {
    url = "https://github.com/rycee/home-manager.git";
    ref = "master";
  };
in {
  imports = [
    # HomeManager
    (import "${home-manager}/nixos")
  ];
}
