{ config, pkgs, lib, ... }:

{
  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";

  # New style
  console.keyMap = "fr";
  console.font = "Lat2-Terminus16";

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = lib.mkDefault "19.09"; # Did you read the comment?
  system.autoUpgrade.enable = true;

  # Systemd conf
  systemd.extraConfig = "DefaultTimeoutStopSec=10s";

  # Enable DConf compatibility
  programs.dconf.enable = true;

  # Enable firewall
  networking.firewall.enable = true;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;
}
