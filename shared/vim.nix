{ config, lib, pkgs, ... }:

{
  # Install other packages
  environment.systemPackages = with pkgs; [ vimHugeX ];

  # Environment variables
  environment.sessionVariables = {
    EDITOR = "vim";
    VISUAL = "vim";
  };

  # Configuration
  home-manager.users.nover.programs.vim = {
    enable = true;
    settings.tabstop = 4;
    settings.smartcase = true;
    plugins = with pkgs.vimPlugins; [
      awesome-vim-colorschemes
      elm-vim
      neocomplete-vim
      rainbow_parentheses-vim
      vim-addon-syntax-checker
      vim-closetag
      vim-json
      vim-markdown
      vim-multiple-cursors
    ];
  };
  home-manager.users.root.programs.vim = {
    enable = true;
    settings.tabstop = 4;
    settings.smartcase = true;
    plugins = with pkgs.vimPlugins; [
      awesome-vim-colorschemes
      elm-vim
      neocomplete-vim
      rainbow_parentheses-vim
      vim-addon-syntax-checker
      vim-closetag
      vim-json
      vim-markdown
      vim-multiple-cursors
    ];
  };
}

