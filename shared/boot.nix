{ config, pkgs, lib, ... }:

with lib; {

  # General boot options
  boot = {
    cleanTmpDir = true;
    kernelPackages = pkgs.linuxPackages_latest;

    # Global configuration
    loader.timeout = mkDefault 1;
    loader.efi.canTouchEfiVariables = mkDefault true;

    # Configure Grub
    loader.grub = {
      enable = mkDefault true;
      configurationLimit = mkDefault 5;
      device = mkDefault "nodev";
      efiSupport = mkDefault true;
    };

    # Enable Plymouth
    plymouth.enable = mkDefault true;
  };
}
