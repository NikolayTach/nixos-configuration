{ config, lib, pkgs, ... }:

{
  # Git default configuration
  home-manager.users.nover.programs.git = {
    userName = "Nicolas Guilloux";
    userEmail = "novares.x@gmail.com";
  };
  home-manager.users.root.programs.git = {
    userName = "Nicolas Guilloux";
    userEmail = "novares.x@gmail.com";
  };

  # Packages
  environment.systemPackages = with pkgs; [
    git
    gitAndTools.lab
    gitAndTools.hub
  ];
}
