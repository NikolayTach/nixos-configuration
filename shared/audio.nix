{ config, pkgs, lib, ... }:

with lib;

{
  config = mkMerge [
    ({
      # Enable sound.
      sound.enable = true;
      hardware.pulseaudio.enable = true;
    })

    (mkIf config.hardware.bluetooth.enable {
      hardware.pulseaudio = {
        package = pkgs.pulseaudioFull.override { jackaudioSupport = true; };
        extraModules = [ pkgs.pulseaudio-modules-bt ];
      };

      # Forward mpris actions
      systemd.user.services.mpris-proxy = {
        description = "Mpris proxy";
        after = [ "network.target" "sound.target" ];
        serviceConfig.ExecStart = "${pkgs.bluez}/bin/mpris-proxy";
        wantedBy = [ "default.target" ];
      };
    })
  ];
}
