{ config, pkgs, ... }:

{
  # User configuration
  users.users.root.shell = pkgs.zsh;
}
