{ config, pkgs, lib, ... }:

{
  # Packages
  environment.systemPackages = with pkgs; [ direnv ];

  # Enable Lorri
  services.lorri.enable = true;
}
