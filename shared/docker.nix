{ config, pkgs, lib, ... }:

{
  # Packages
  environment.systemPackages = with pkgs; [ docker-compose ];

  # Enable Docker
  virtualisation.docker.enable = true;

  # Shell aliases
  environment.shellAliases = {
    # Docker compose
    "dc" = "docker-compose";
    "dcea" = "docker-compose exec application";
    "dup" = "docker-compose up -d";
    "ddown" = "docker-compose down --remove-orphans";
    "dr" = "ddown; dup";
    "dui" =
      "docker images |grep -v REPOSITORY|awk '{print $1}'|xargs -L1 docker pull";
  };
}
