{ config, lib, pkgs, ... }:

{
  # User configuration
  users.users.nover = {
    description = "Nicolas Guilloux";
    uid = 1000;
    isNormalUser = true;
    createHome = true;
    initialPassword = "nover";
    shell = pkgs.zsh;
    group = "users";
    extraGroups = [ "docker" "input" "networkmanager" "nover" "power" "wheel" ];
    openssh.authorizedKeys.keys = [
      # Nover RichCongress
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG7lrPthNbMa6mmqiDPCZbOZZXudGX+vBFDPYaB8ugf+ nguilloux@richcongress.com"
      # Nover Zenbook
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHRZj2kxKpNjrh092tDeF85+vpzsud/vbHoZDhzsTE4u nicolas.guilloux@protonmail.com"
    ];
  };
}
