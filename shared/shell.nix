{ config, lib, pkgs, ... }:

{
  # Install other packages
  environment.systemPackages = with pkgs; [
    alacritty
    imagemagick
    pciutils
    usbutils
    exa
    bat
    fd
    gimp
    ripgrep-all
    unzip
    vimHugeX
    wget
    zip
    zstd
  ];

  # Enable ZSH
  programs.zsh = {
    enable = true;
    autosuggestions.enable = true;
    syntaxHighlighting.enable = true;
    histSize = 9999;
    promptInit = builtins.readFile ../assets/zshrc.sh;
    ohMyZsh = {
      enable = true;
      plugins = [ "git" "sudo" ];
      theme = "agnoster";
    };
  };

  # Make ZSH the default shell
  users.defaultUserShell = pkgs.zsh;

  # Aliases
  environment.shellAliases = {
    "ls" = "exa --color-scale --group-directories-first -l -a";
    "tree" = "exa -T";
    "grep" = "rga";
    "find" = "fd";
    "cat" = "bat";
    "nrs" = "sudo nixos-rebuild switch --show-trace";
    "nrsu" = "sudo nixos-rebuild switch --upgrade --show-trace";
    "ncg" = "sudo nix-collect-garbage";

    # Go to
    "gt-config" = "cd ~/.nixos";
    "config" = "codium ~/.nixos";
  };

  # Environment variables
  environment.sessionVariables = {
    EDITOR = "vim";
    VISUAL = "vim";
  };

  # Fonts
  fonts.fonts = with pkgs; [ powerline-fonts ];
}

