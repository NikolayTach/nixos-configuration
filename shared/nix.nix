{ config, pkgs, ... }:

{
  # Automatic garbage collect
  nix.gc = {
    automatic = true;
    options = "--delete-older-than 15d";
  };

  # List packages installed in system profile.
  environment.systemPackages = with pkgs; [ nixfmt nox ];
}
