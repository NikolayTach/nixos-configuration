{ config, pkgs, lib, ... }:

with lib;
with types;

{
  options.global = {
    podman.enable = mkOption {
      type = bool;
      default = false;
      description = "Enables Podman (and Docker compatibility layer)";
    };
  };
}
