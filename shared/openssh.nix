{ config, pkgs, lib, ... }:

{
  # Enable the OpenSSH daemon.
  services.openssh.enable = lib.mkDefault true;
  services.openssh.passwordAuthentication = lib.mkDefault true;
}
