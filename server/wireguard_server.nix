{ config, pkgs, ... }:

{
  # enable NAT
  networking.nat.enable = true;
  networking.nat.externalInterface = "enp0s20f0";
  networking.nat.internalInterfaces = [ "wireguard0" ];
  networking.firewall.allowedUDPPorts = [ 51820 ];

  environment.systemPackages = [ pkgs.wireguard ];

  networking.wireguard.interfaces.wireguard0 = {
    # Determines the IP address and subnet of the server's end of the tunnel interface.
    ips = [ "10.100.0.1/24" ];

    # The port that Wireguard listens to. Must be accessible by the client.
    listenPort = 51820;

    # This allows the wireguard server to route your traffic to the internet and hence be like a VPN
    # For this to work you have to set the dnsserver IP of your router (or dnsserver of choice) in your clients
    postSetup = ''
      ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s 10.100.0.0/24 -o enp0s20f0 -j MASQUERADE
    '';

    # This undoes the above command
    postShutdown = ''
      ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s 10.100.0.0/24 -o enp0s20f0 -j MASQUERADE
    '';

    # Path to the private key file.
    #
    # Note: The private key can also be included inline via the privateKey option,
    # but this makes the private key world-readable; thus, using privateKeyFile is
    # recommended.
    privateKeyFile = "/home/nover/.wireguard-keys/private";

    peers = [
      { # NoverRichCongress
      publicKey = "KuoU1JI3JcuKUAXeryJGkZZTioZLc/K959ak1mhu/To=";
      allowedIPs = [ "10.100.0.2/32" ];
      }
      { # Nover Mi Mix 3
      publicKey = "qmJ2tDeTM/40lzlpOnUYTmKU7wTME9ZTd8UCEoHzhTA=";
      allowedIPs = [ "10.100.0.3/32" ];
      }
    ];
  };
}
