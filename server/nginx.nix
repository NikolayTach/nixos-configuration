{ config, nixpkgs, ... }:

let
  proxy = url: {
    proxyPass = url;
    proxyWebsockets = true;
    extraConfig =
      # required when the target is also TLS server with multiple hosts
      "proxy_ssl_server_name on;" +
      # required when the server wants to use HTTP Authentication
      "proxy_pass_header Authorization;";
  };
in {
  # Firewall ports
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  # ACME Challenge
  security.acme.acceptTerms = true;
  security.acme.email = "nicolas.guilloux@protonmail.com";

  # Nginx configuration
  services.nginx = {
    enable = true;

    # Use recommended settings
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    virtualHosts."bobarr.nicolasguilloux.eu" = {
      forceSSL = true;
      enableACME = true;
      basicAuthFile = ../assets/bobarrPassword;

      locations."/" = proxy "http://127.0.0.1:3000";
      locations."/api/" = proxy "http://127.0.0.1:4000/";
      locations."/jackett" = proxy "http://127.0.0.1:9117/jackett";
    };
  };
}
