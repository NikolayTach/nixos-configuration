# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let inherit (import ../utilities/scanner.nix { inherit lib; }) importNixFolder;
in {
  imports = [
    # Include the results of the hardware scan.
    ./../../hardware-configuration.nix
  ] ++ (importNixFolder ../server) ++ (importNixFolder ../shared);

  # Boot
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.efiSupport = false;
  boot.plymouth.enable = false;
  boot.kernelParams = [ "boot.shell_on_fail" ];
  boot.loader.grub.storePath = "/nixos/nix/store";
  boot.initrd.supportedFilesystems = [ "ext4" ];
  boot.initrd.postDeviceCommands = ''
    mkdir -p /mnt-root/old-root ;
    mount -t ext4 /dev/sda2 /mnt-root/old-root ;
  '';

  # Filesystem
  fileSystems = {
    "/" = {
      device = "/old-root/nixos";
      fsType = "none";
      options = [ "bind" ];
    };
    "/old-root" = {
      device = "/dev/sda2";
      fsType = "ext4";
    };
  };

  # Networking
  networking.hostName = "Nover-Server"; # Define your hostname.

  # Security
  security.sudo.wheelNeedsPassword = false;

  # Welcome message
  users.motd = ''
          _   _                     _____                          
         | \ | |                   /  ___|                         
         |  \| | _____   _____ _ __\ `--.  ___ _ ____   _____ _ __ 
         | . ` |/ _ \ \ / / _ \ '__|`--. \/ _ \ '__\ \ / / _ \ '__|
         | |\  | (_) \ V /  __/ |  /\__/ /  __/ |   \ V /  __/ |   
         \_| \_/\___/ \_/ \___|_|  \____/ \___|_|    \_/ \___|_|   

    Welcome to Nover's server. It runs NixOS, so you can install software
      using `nix-env -iA nixpkgs.<the-package>` and let the magic happen!

  '';

  # Global configuration
  global.podman.enable = true;
}

