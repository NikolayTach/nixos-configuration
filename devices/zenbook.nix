# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let inherit (import ../utilities/scanner.nix { inherit lib; }) importNixFolder;
in {
  imports = [
    # Include the results of the hardware scan.
    ./../../hardware-configuration.nix
  ] ++ (importNixFolder ../desktop) ++ (importNixFolder ../shared);

  # Boot with Systemd
  boot.loader.grub = {
    default = 1;
    extraEntries = ''
      menuentry "ChromeOS (boot from disk image)" {
        img_part=/dev/sda3
        img_path=/chromos.img
        search --no-floppy --set=root --file $img_path
        loopback loop $img_path
        linux (loop,gpt7)/kernel boot=local noresume noswap loglevel=7 disablevmx=off \
          cros_secure cros_debug loop.max_part=16 img_part=$img_part img_path=$img_path
        initrd (loop,gpt7)/initramfs.img
      }
    '';
  };

  # Networking
  networking.hostName = "Nover-Zenbook";
  networking.useDHCP = false;
  networking.interfaces.wlp1s0.useDHCP = true;

  # Enable Intel Microcode
  hardware.cpu.intel.updateMicrocode = true;

  # Force iHD driver for Shadow
  programs.shadow-client.forceDriver = "iHD";

  # Configuration
  global.gnome.enable = true;
}

