# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let inherit (import ../utilities/scanner.nix { inherit lib; }) importNixFolder;
in {
  imports = [
    # Include the results of the hardware scan.
    ./../../hardware-configuration.nix
  ] ++ (importNixFolder ../desktop) ++ (importNixFolder ../shared);

  # Use the GRUB 2 boot loader.
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.efiSupport = false;

  # Hostname
  networking.hostName = "Nover-VirtualBox";

  # VirtualBox Stuff
  services.xserver.videoDriver = "virtualbox";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s3.useDHCP = true;
}

