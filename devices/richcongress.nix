# Edit this configuration file to define what should be installed on
# your system.	Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

let inherit (import ../utilities/scanner.nix { inherit lib; }) importNixFolder;
in {
  imports = [
    # Include the results of the hardware scan.
    ./../../hardware-configuration.nix
  ] ++ (importNixFolder ../desktop) ++ (importNixFolder ../shared);

  boot.loader.grub.extraEntries = ''
    menuentry "Shadow" {
        insmod chain
        chainloader /EFI/shadow/shadow-bootx64.efi
    }
  '';

  # Hostname
  networking.hostName = "NoverDesktop"; # Define your hostname.
  networking.useDHCP = false;
  networking.interfaces.enp37s0.useDHCP = true;

  # AMD CPU
  hardware.cpu.amd.updateMicrocode = true;

  # AMD GPU
  boot.kernelModules = [ "amdgpu" ];
  environment.systemPackages = with pkgs; [ xorg.xf86videoamdgpu ];
  services.xserver.videoDrivers = [ "amdgpu" ];

  # Bluetooth
  hardware.bluetooth.enable = true;
  hardware.logitech.wireless.enable = true;
  hardware.logitech.wireless.enableGraphical = true;

  # Configuration
  global.gnome.enable = true;
  global.podman.enable = false;
  richcongress.enable = true;
  global.wakeonlan.interface = "enp37s0";
}
