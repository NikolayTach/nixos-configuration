{ config, pkgs, ... }:

{
  # Add packages
  environment.systemPackages = with pkgs; [ hydrogen qjackctl ];

  # Jack related configuration
  services.jack.jackd.enable = false;
  boot.kernelModules = [ "snd-seq" "snd-rawmidi" ];
  users.users.nover.extraGroups = [ "jackaudio" ];
}
