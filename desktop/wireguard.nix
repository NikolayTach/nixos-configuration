{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [ wireguard pkgs.mosh ];

  # Enable Mosh
  programs.mosh.enable = true;

  # Enable Wireguard
  networking.wireguard.interfaces.wireguard0 = {
    # Determines the IP address and subnet of the client's end of the tunnel interface.
    ips = [ "10.100.0.2/24" ];

    # Path to the private key file.
    # Note: The private key can also be included inline via the privateKey option,
    # but this makes the private key world-readable; thus, using privateKeyFile is
    # recommended.
    privateKeyFile = "/home/nover/.wireguard-keys/private";

    peers = [
      # For a client configuration, one peer entry for the server will suffice.
      {
        # Public key of the server (not a file path).
        publicKey = "DdGJ31V+QdZxPJ1rIiBVxZtNK5CeIVcQfzeN+qvhLGg=";

        # Forward all the traffic via VPN.
        # allowedIPs = [ "0.0.0.0/0" ];

        # Or forward only particular subnets
        allowedIPs = [ "10.100.0.2/32" ];

        # Set this to the server IP and port.
        endpoint = "51.15.7.95:51820";

        # Send keepalives every 25 seconds. Important to keep NAT tables alive.
        persistentKeepalive = 25;
      }
    ];

  };
}
