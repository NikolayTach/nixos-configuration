{ config, pkgs, lib, ... }:

with lib;

{
  config = (mkIf config.global.kde.enable {
    environment.systemPackages = with pkgs; [
      hicolor-icon-theme
      ark
      kdeconnect
      latte-dock
      plasma-integration
      plasma-browser-integration
      gwenview
      kdeFrameworks.kirigami2
      kdeApplications.kget
      kdeApplications.spectacle
      partition-manager
      libnotify
      vlc
      qt5.qtquickcontrols2
      qt5.qtquickcontrols
      gnome2.gnome_icon_theme
      milou
    ];

    # Enable XServer
    services.xserver = {
      enable = true;
      layout = "fr";
      xkbOptions = "eurosign:e";
      startDbusSession = false;
    };

    # Enable SDDM
    services.xserver.displayManager.sddm = {
      enable = true;
      autoNumlock = true;
    };

    # Pick KDE as DesktopManager
    services.xserver.desktopManager.plasma5.enable = true;

    # Enable network manager
    networking.networkmanager.enable = true;

    # Services
    services.dbus.enable = true;
    services.dbus.socketActivated = true;
  });
}
