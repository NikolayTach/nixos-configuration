{ config, pkgs, lib, ... }:

let
  # Youtube
  youtube = pkgs.makeDesktopItem {
    name = "youtube";
    exec =
      "chromium --noerrdialogs --disable-translate --no-first-run --disable-infobars --app=https://youtube.fr";
    desktopName = "YouTube";
    categories = "AudioVideo";
    icon = (builtins.fetchurl
      "http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/social-youtube-icon.png");
  };

  # Plex
  plex = pkgs.makeDesktopItem {
    name = "plex";
    exec =
      "chromium --noerrdialogs --disable-translate --no-first-run --disable-infobars --app=https://app.plex.tv/desktop";
    desktopName = "Plex";
    categories = "AudioVideo";
    icon = (builtins.fetchurl
      "https://beyondtv.com/wp-content/uploads/2020/04/910-9103810_plex-media-server-transparent-plex-icon.png");
  };

  # Netflix
  netflix = pkgs.makeDesktopItem {
    name = "netflix";
    exec =
      "chromium --noerrdialogs --disable-translate --no-first-run --disable-infobars --app=https://netflix.com";
    desktopName = "Netflix";
    categories = "AudioVideo";
    icon = (builtins.fetchurl
      "https://www.flaticon.com/premium-icon/icons/svg/2504/2504929.svg");
  };

  # NixOS Configuration
  configEdition = pkgs.makeDesktopItem {
    name = "nixos_config";
    exec = "codium /etc/nixos/config";
    comment = "Open the NixOS configuration with Codium";
    desktopName = "NixOS Config";
    genericName = "Configuration";
    categories = "Development";
    icon =
      (builtins.fetchurl "https://nixos.org/logo/nixos-logo-only-hires.png");
  };
in {
  # Packages
  environment.systemPackages = with pkgs; [
    youtube
    netflix
    plex
    configEdition
  ];
}
