{ config, pkgs, ... }:

{
  # Packages
  environment.systemPackages = with pkgs; [ sidequest ];

  # Enable ADB
  programs.adb.enable = true;
}
