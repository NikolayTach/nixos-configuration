{ config, pkgs, lib, ... }:

{
  # Packages
  environment.systemPackages = with pkgs; [ _1password-gui ];
}
