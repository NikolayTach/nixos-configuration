{ config, pkgs, ... }:

let
  repo = fetchGit {
    url = "https://github.com/Elyhaka/shadow-nix";
    ref = "master";
  };
  # repo = /home/nover/Git/shadow-nix;
  repoTest = /home/nover/Git/shadow-nix-test;

  nixPackage = repo + "/shadow-package.nix";
  nixTestPackage = repoTest + "/shadow-package.nix";
  nixSystemPackage = repo + "/system.nix";

  # Drirc file
  drirc = (fetchGit {
    url = "https://github.com/NicolasGuilloux/blade-shadow-beta";
    ref = "master";
  } + "/resources/drirc");
in {
  # Import Shadow prod
  imports = [ nixSystemPackage ];

  # Provides the `vainfo` command
  environment.systemPackages = with pkgs; [
    libva-utils
    libva
    (pkgs.callPackage nixPackage { shadowChannel = "preprod"; })
    (pkgs.callPackage nixPackage { shadowChannel = "testing"; })
    (pkgs.callPackage nixTestPackage { shadowChannel = "insider"; })
  ];

  # Hardware hybrid decoding
  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };

  # Default channel
  programs.shadow-client.channel = "prod";

  # Hardware drivers
  hardware.opengl.enable = true;
  hardware.opengl.extraPackages = with pkgs; [
    vaapiIntel
    vaapiVdpau
    libva-full
    libGL
    mesa
    libvdpau-va-gl
    intel-media-driver
  ];

  # Add GPU fixes
  environment.etc."drirc".source = drirc;
}
