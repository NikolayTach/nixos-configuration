{ config, pkgs, lib, ... }:

with lib;

{
  config = mkMerge [
    # Add Kmail for KDE
    (mkIf config.global.kde.enable {
      environment.systemPackages = with pkgs; [ kmail thunderbird ];
    })

    # Add Geary for Gnome
    (mkIf config.global.gnome.enable { programs.geary.enable = true; })

    # Add Global config
    ({
      # Add Local smtp/imap for ProtonMail
      environment.systemPackages = with pkgs; [ hydroxide ];

      # Starts Hydroxide IMAP server
      systemd.services."hydroxide-imap" = {
        description = "Start Hydroxide IMAP server";
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          Slice = "machine.slice";
          ExecStart = "${pkgs.hydroxide}/bin/hydroxide imap";
          Restart = "on-failure";
          RestartSec = "10s";
        };
      };

      # Starts Hydroxide SMTP server
      systemd.services."hydroxide-smtp" = {
        description = "Start Hydroxide SMTP server";
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          Slice = "machine.slice";
          ExecStart = "${pkgs.hydroxide}/bin/hydroxide smtp";
          Restart = "on-failure";
          RestartSec = "10s";
        };
      };
    })
  ];
}
