{ config, pkgs, ... }:

{
  # Packages
  environment.systemPackages = with pkgs; [ xorg.libxcb steam ];

  # Hardware compatibility
  hardware.opengl.driSupport32Bit = true;
  hardware.pulseaudio.support32Bit = true;
}
