{ config, pkgs, lib, ... }:

with lib;

{
  config = (mkIf config.global.gnome.enable {
    # Packages
    environment.systemPackages = with pkgs; [
      hicolor-icon-theme
      gnome3.adwaita-icon-theme
      gnome3.gnome-tweaks
      gnome3.gjs
      gnome3.gnome-books
      chrome-gnome-shell
      libappindicator
      libnotify
      gnomeExtensions.appindicator
      gnomeExtensions.dash-to-dock
      gnomeExtensions.dash-to-panel
      gnomeExtensions.caffeine
      gnomeExtensions.gsconnect
      gnome3.gnome-backgrounds
      gnome3.dconf-editor
    ];

    # Enable XServer
    services.xserver = {
      enable = true;
      layout = "fr";
      xkbOptions = "eurosign:e";
    };

    # Enable GDM
    services.xserver.displayManager.gdm.enable = true;
    services.xserver.displayManager.lightdm.background =
      "${pkgs.gnome3.gnome-backgrounds}/share/backgrounds/gnome/adwaita-night.jpg";

    # Pick Gnome as DesktopManager
    services.xserver.desktopManager = {
      xterm.enable = false;
      gnome3 = {
        enable = true;
        sessionPath = [ pkgs.packagekit ];
        extraGSettingsOverrides = ''
          [org.gnome.desktop.background]
          picture-uri="file://${pkgs.gnome3.gnome-backgrounds}/share/backgrounds/gnome/adwaita-night.jpg"

          [org.gnome.desktop.screensaver]
          picture-uri="file://${pkgs.gnome3.gnome-backgrounds}/share/backgrounds/gnome/adwaita-night.jpg"

          [org.gnome.shell.weather]
          locations="[<(uint32 2, <('Bordeaux', 'LFBD', true, [(0.78248927549302705, -0.012217304763960306)], [(0.78248927549302705, -0.0098902049123987066)])>)>]"
        '';
      };
    };

    # Make sure the daemon is enabled
    services.udev.packages = with pkgs; [ gnome3.gnome-settings-daemon ];

    # Allow GSConnect
    networking.firewall = {
      allowedTCPPortRanges = [{
        from = 1714;
        to = 1764;
      }];
      allowedUDPPortRanges = [{
        from = 1714;
        to = 1764;
      }];
    };

    # Enable touchpad support.
    services.xserver.libinput.enable = true;

    # Gnome services
    services.gnome3 = {
      chrome-gnome-shell.enable = true;
      gnome-keyring.enable = true;
    };
  });
}
