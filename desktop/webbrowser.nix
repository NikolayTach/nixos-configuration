{ config, pkgs, lib, ... }:

{
  # Packages
  environment.systemPackages = with pkgs; [ chromium firefox ];

  # Enable touch support for Firefox
  environment.variables.MOZ_USE_XINPUT2 = [ "1" ];

  # Block ads
  networking.extraHosts = builtins.readFile (builtins.fetchurl
    "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts");
}
