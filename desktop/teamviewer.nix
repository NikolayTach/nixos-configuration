{ config, pkgs, ... }:

{
  # Enable TeamViewer
  services.teamviewer.enable = false;
}
