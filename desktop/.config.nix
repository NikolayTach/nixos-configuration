{ config, pkgs, lib, ... }:

with lib;
with types;

{
  options.global = {
    kde.enable = mkOption {
      type = bool;
      default = false;
      description = "Enables KDE desktop";
    };

    gnome.enable = mkOption {
      type = bool;
      default = false;
      description = "Enables Gnome desktop";
    };

    wakeonlan.interface = mkOption {
      type = str;
      default = "";
      description = "Network card that will receive the magic packet";
    };
  };
}
