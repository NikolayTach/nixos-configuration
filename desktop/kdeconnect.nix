{ config, pkgs, ... }:

{
  # Packages
  environment.systemPackages = with pkgs; [ kdeconnect ];

  # Allow KDEConnect
  networking.firewall = {
    allowedTCPPortRanges = [{
      from = 1714;
      to = 1764;
    }];
    allowedUDPPortRanges = [{
      from = 1714;
      to = 1764;
    }];
  };
}
