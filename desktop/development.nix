{ config, pkgs, lib, ... }:

{
  # Packages
  environment.systemPackages = with pkgs; [
    jetbrains.phpstorm
    postman
    vscodium
    ngrok
  ];

  environment.variables.ENV = [ "dev" ];

  networking.hosts."127.0.0.1" = [
    "shadow.test"
    "pma.shadow.test"
    "config.nix.test"
    "pma.nix.test"
    "es.nix.test"
    "kibana.nix.test"
    "flex.test"
  ];

  # Aliases
  environment.shellAliases = {
    # In container
    "composer" = "dcea php -d memory_limit=-1 /usr/local/bin/composer";
    "npm" = "dcea npm";
    "yarn" = "dcea yarn";

    # Webdev
    "console" = "dcea php bin/console";
    "encore" = "dcea ./node_modules/.bin/encore dev";
    "dep" = "dcea ./vendor/bin/dep";
    "phptest" =
      "dcea php ./vendor/phpunit/phpunit/phpunit --log-junit coverage/junit.xml";
    "coverage" =
      "dcea php -d zend_extension=xdebug.so ./vendor/phpunit/phpunit/phpunit --prepend phpunit-filter.php --configuration phpunit.xml.dist --coverage-text --coverage-html coverage --log-junit coverage/junit.xml";
  };

  # Fonts with ligature
  fonts.fonts = with pkgs; [ fira-code ];

  # Starts Nginx Proxy
  systemd.services."nginx-proxy" = lib.mkIf config.global.podman.enable {
    description = "Start Nginx Proxy";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      Slice = "machine.slice";
      ExecStart = ''
        ${pkgs.docker}/bin/docker run \
        -p 80:80 \
        -v /var/run/docker.sock:/tmp/docker.sock:ro \
        --rm \
        --name nginx-proxy \
        --net proxy \
        --privileged \
        --userns=host richcongress/nginx-proxy
      '';
      ExecStop = "${pkgs.podman}/bin/podman stop nginx-proxy";
      Restart = "on-failure";
      RestartSec = "10s";
    };
  };
}
