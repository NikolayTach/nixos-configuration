{ config, pkgs, lib, ... }:

let wolInterface = config.global.wakeonlan.interface;
in with lib; {
  config = mkIf (wolInterface != "") {
    networking.firewall.allowedUDPPorts = [ 9 ];
    environment.systemPackages = [ pkgs.ethtool ];

    services.wakeonlan.interfaces = [{
      interface = wolInterface;
      method = "magicpacket";
    }];

    services.cron.enable = true;
    services.cron.systemCronJobs =
      [ "@reboot root ${pkgs.ethtool}/sbin/ethtool -s ${wolInterface} wol g" ];
  };
}
