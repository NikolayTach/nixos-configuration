{ config, pkgs, ... }:

{
  users.users.jeedom = {
    createHome = false;
    description = "Jeedom interface";
    extraGroups = [ ];
    isSystemUser = true;
    useDefaultShell = true;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIcfdhNsu3b1NuTj/EmV14/KdC3TcmHkwAi1aLix3UpT NoverBox"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfkwtyASWn9ju1zxILJmw9fAWOw1jygx5yeR4wlrK5rnPaAcvw5gQ/FQc1kckRNZJb61tTrf75Ec9sKIcbY/cJeKEdXoLYiOt7NdosHRmdGnFzfiD8/0dtLd0GMg3vEMnMKIfiEWquSdec5jXimkjfuRp+EZpbMXj6EX7noII0VKjb6iKp4lLT7ZiWLblCGjxKwvJGZ2NOE5VkmT0nnvDZlQIMycvLTYXzVskH4gQY1c4c7qYdjAafaQzPXV+sfOFv3GkgOzwxWJZwi0RtfzsERfY0QL85ZSoQ1RhgMCpFYP0iHnkryARutungXyMznOQF8zgjwfmkL0yrOZRJu2s5 www-data@NoverPi"
    ];
  };

  security.sudo.extraConfig =
    "jeedom ALL=(ALL) NOPASSWD: /usr/bin/env systemctl poweroff, /usr/bin/env systemctl suspend";
}
