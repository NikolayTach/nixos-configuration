{ config, pkgs, ... }:

{
  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    drivers = [ pkgs.cnijfilter2 ];
  };

  # Enables network recognition
  services.avahi = {
    enable = true;
    nssmdns = true;
  };
}
