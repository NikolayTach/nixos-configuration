{ config, pkgs, lib, ... }:

let
  local = true;

  richCongressConfiguration = if !local then
    import (builtins.fetchGit {
      url =
        "ssh://git@gitlab.richcongress.io/richcongress/nix-configuration.git";
      ref = "master";
    })
  else
    /home/nover/Git/RichCongressNixConfig;

  projectsFolder = "/home/nover/Projets";
in {
  imports = [ richCongressConfiguration ];

  richcongress = {
    fonts.firaCode = true;
    fonts.jetbrainsMono = true;

    projects.apajh = {
      name = "Apajh";
      path = "${projectsFolder}/APAJH";
      git = "ssh://git@gitlab.richcongress.io/private/apajh/apajh.git";
      host = "expressions.apajh";
      icon = "/assets/images/picto-accompanied-person.png";
      enableShortcut = true;
    };

    projects.fafih_training = {
      name = "Fafih Offre Formation";
      path = "${projectsFolder}/FAFIH-TRAINING";
      git = "ssh://git@gitlab.richcongress.io/private/fafih/fafih-training.git";
      host = "fafih";
      icon = "/assets/images/new-training.png";
      enableShortcut = true;
    };

    projects.fafih_sso = {
      name = "Fafih Mon espace";
      path = "${projectsFolder}/FAFIH-SSO";
      git = "ssh://git@gitlab.richcongress.io/private/fafih/fafih-sso.git";
      host = "fafih-sso";
      icon = "/assets/images/favicon.ico";
      enableShortcut = true;
    };

    projects.hellorse = {
      name = "HelloRSE";
      path = "${projectsFolder}/HELLORSE";
      git = "ssh://git@gitlab.richcongress.io/private/hellorse.git";
      host = "hellorse";
      icon =
        "/themes/HelloRSETheme/assets/images/logo/logo-hellorse-vertical.svg";
      enableShortcut = true;
    };

    projects.opcalia = {
      name = "Opcalia";
      path = "${projectsFolder}/OPCALIA";
      git =
        "ssh://git@gitlab.richcongress.io/private/opcalia/opcalia_symfony.git";
      host = "opcalia";
      icon = "/assets/images/favicon.ico";
      enableShortcut = true;
    };

    projects.opcats = {
      name = "Opcats Campus Compétences";
      path = "${projectsFolder}/OPCATS";
      git = "ssh://git@gitlab.richcongress.io/private/opcats/opcats.git";
      host = "campus-competences";
      icon = "/assets/images/logo-cc.svg";
      enableShortcut = true;
    };

    projects.staff = {
      name = "Staff";
      path = "${projectsFolder}/STAFF";
      git = "ssh://git@gitlab.richcongress.io/richcongress/staff.git";
      host = "staff";
      icon = "/assets/images/logo.png";
      enableShortcut = true;
    };

    projects.ostar = {
      name = "Ostar";
      path = "${projectsFolder}/OSTAR";
      git = "ssh://git@gitlab.richcongress.io/richcongress/ostar.git";
      host = "ostar";
      icon = "/client/src/assets/logo.svg";
      enableShortcut = true;
    };

    projects.referentiel = {
      name = "Referentiel.io";
      path = "${projectsFolder}/REFERENTIEL";
      git = "ssh://git@gitlab.richcongress.io/richcongress/referentiel-v2.git";
      host = "referentiel";
      icon = "/assets/images/favicon.ico";
      enableShortcut = true;
    };
  };

  services.openvpn.servers.RichCongressVPN =
    lib.mkIf config.richcongress.enable {
      config = " config /home/nover/Documents/nicolas.ovpn ";
    };
}
