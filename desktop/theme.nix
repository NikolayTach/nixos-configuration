{ config, pkgs, lib, ... }:

with lib;

{
  # Fonts
  fonts.fonts = with pkgs; [
    noto-fonts
    noto-fonts-emoji
    twemoji-color-font
    papirus-icon-theme
    numix-cursor-theme
  ];

  # Files
  home-manager.users.nover.home.file = {
    ".assets".source = ../assets/images;
    ".face".source = ../assets/images/avatar.jpg;
  };
}
