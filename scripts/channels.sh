#!/usr/bin/env sh
# bash <(curl https://gitlab.com/NicolasGuilloux/nixos-configuration/-/raw/master/scripts/channels.sh)

RELEASE=20.03

# Delete previous
nix-channel --remove home-manager
nix-channel --remove nixos
nix-channel --remove nixpkgs
nix-channel --remove unstable


# Add the channels
nix-channel --add "https://github.com/rycee/home-manager/archive/release-$RELEASE.tar.gz" home-manager
nix-channel --add "https://nixos.org/channels/nixos-$RELEASE" nixpkgs
nix-channel --add "https://nixos.org/channels/nixos-unstable" unstable

# Update channels
nix-channel --update