#!/usr/bin/env sh
# bash <(curl https://gitlab.com/NicolasGuilloux/nixos-configuration/-/raw/master/scripts/rescue.sh)

eval HOMEDIR=~
CONFIG_PATH="$HOMEDIR/configuration.nix"

# Mounting
echo "\nMount everything"
sudo swapon /dev/sda3
sudo mount /dev/sda2 /mnt
mkdir /mnt/boot
sudo mount /dev/sda1 /mnt/boot

# Add users
echo "\nAdding the various users used for the installation"
sudo groupadd nixbld
sudo useradd nixbld1
sudo useradd nixbld2
sudo useradd nixbld3
sudo useradd nixbld4
sudo gpasswd -a nixbld1 nixbld
sudo gpasswd -a nixbld2 nixbld
sudo gpasswd -a nixbld3 nixbld
sudo gpasswd -a nixbld4 nixbld

# Install dependencies
echo "\nInstall the dependencies"
sudo apt install bzip2 btrfs-tools

# Fallback for the config path
cat <<EOF > $CONFIG_PATH
{ fileSystems."/" = {};
    boot.loader.grub.enable = false;
} 
EOF

# Install Nix
echo "\nInstalling Nix"
wget https://nixos.org/nix/install -O /tmp/nixos_install
bash /tmp/nixos_install
. $HOMEDIR/.nix-profile/etc/profile.d/nix.sh

# Add and update channels
echo "\nUpdating channels"
wget https://gitlab.com/NicolasGuilloux/nixos-configuration/-/raw/master/scripts/channels.sh -O /tmp/channels_update
bash /tmp/channels_update

# Export variables
export NIX_PATH=nixpkgs=$HOMEDIR/.nix-defexpr/channels/nixos:nixos=$HOMEDIR/.nix-defexpr/channels/nixos/nixos
export NIXOS_CONFIG=$CONFIG_PATH

# Install Nix dependencies
echo "\nInstalling Nix dependencies"
nix-env -i -A config.system.build.nixos-install -A config.system.build.nixos-option -A config.system.build.nixos-generate-config -f "<nixos>"

# Command to execute by the user himself
echo "\n\n/\!\ You need to execute the following command to use the Nix programs in the terminal:"
echo "    . $HOMEDIR/.nix-profile/etc/profile.d/nix.sh\n"